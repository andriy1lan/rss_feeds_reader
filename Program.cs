﻿using System;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

using ConsoleRssFeedReader.Handlers;

namespace ConsoleRssFeedReader
{
    class Program
    {
        public static string configFile=@"C:\RssFeedConfig.xml";
        
        public static void Main(string[] args)
        {
            Console.WriteLine("HERE is CONSOLE RSS FEEDS READER");
            Console.WriteLine("The next are current Feed names");
            XMLHandler.GetAllFeedNames(configFile);
            Console.WriteLine("===============================");
            //XMLHandler.GetAllFeedUrls(configFile);
            string req = "Please enter  the command to act on RSS Feed"+
            "\nif you wanna add new feed - enter - AD, if to remove feed"+
            "\ntype - RE - if to download some specific feeds - type - DO -"+
            "\nif to close the app - type EXIT and then click ENTER";
            Console.WriteLine(req);
             while (true)
            {
                 Console.WriteLine("Enter the Command for Rss Feed - AD, RE, DO");
                try
                {
                    string command = Console.ReadLine();
                    if (command.Equals("EXIT", StringComparison.InvariantCultureIgnoreCase))
                        break;

                    else if (command.Equals("AD", StringComparison.InvariantCultureIgnoreCase))
                        { 
                        Console.WriteLine("ADDD");
                        WriteNewFeed();
                        }
                    else if (command.Equals("RE", StringComparison.InvariantCultureIgnoreCase))
                        { 
                        RemoveFeed();
                        }
                    else if (command.Equals("DO", StringComparison.InvariantCultureIgnoreCase))
                        { 
                        DownloadFeeds();
                        }
                }

                catch (IOException ex)
                {
                    Console.WriteLine(ex.InnerException.Message);
                }
                finally
                {
                    Console.WriteLine("===================");
                }

            }
        }

        public static void WriteNewFeed() {
            string name=null;
            string url=null;
            string addRequest = "Please insert the name and then after"+
            "\nENTER key the url of RSS Feed. It will be saved automatically"+
            "\nin XML congig file";
            Console.WriteLine(addRequest);
            Console.WriteLine("Type the Feed name: ");
            name = Console.ReadLine();
            Console.WriteLine("Type the Feed url: ");
            url = Console.ReadLine();
            XMLHandler.AddFeed(name,url,configFile);
            Console.WriteLine("Feed Added: "+name+"--"+url);
        }

        public static void RemoveFeed() {
            string name=null;
            string removeRequest = "Please type the name of RSS Feed to remove"+
            "\nand then press ENTER key - it will be removed from xml congig file.";
            Console.WriteLine(removeRequest);
            Console.WriteLine("Type the Feed name: ");
            name = Console.ReadLine();
            XMLHandler.RemoveFeed(name,configFile);
            Console.WriteLine("Feed Removed: "+name);
        }

        public static void DownloadFeeds() {
            string downloadNumber = "Please type the number of RSS Feeds (5-10)"+
            "\nto review and then press ENTER key.";
            Console.WriteLine(downloadNumber);
            string number = Console.ReadLine();
            try{
            int num=Int32.Parse(number);
            }
            catch (FormatException fe) {
               Console.WriteLine(fe.Message);
            }
            string downloadFeeds = "Please type the several RSS Feeds names (5-10)"+
            "\nseparated by comma and then press ENTER key.";
            Console.WriteLine(downloadFeeds);
            string feedNames= Console.ReadLine();
            string [] feedNamesArray=feedNames.Split(",",StringSplitOptions.RemoveEmptyEntries);
            feedNamesArray=feedNamesArray.Select(u=>u.Trim()).ToArray();
            XMLHandler.Download(feedNamesArray, configFile);
        }
    }
}
