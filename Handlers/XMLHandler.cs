using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Text;
using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel.Syndication;

namespace ConsoleRssFeedReader.Handlers
{
    public class XMLHandler
    {
       public static void AddFeed (string name, string url, string fileName) {
            bool fileExists=File.Exists(fileName);
            Console.WriteLine(fileExists);
            try{
            XmlDocument doc = new XmlDocument();
            XmlNode rootNode;
            if (fileExists) { 
            doc.Load(fileName);
            rootNode=doc.DocumentElement;}
            else { rootNode = doc.CreateElement("feeds");
            doc.AppendChild(rootNode);
            }
            XmlNode feedNode = doc.CreateElement("feed");
            XmlNode nameNode = doc.CreateElement("name");
            nameNode.InnerText = name;
            feedNode.AppendChild(nameNode);
            XmlNode urlNode = doc.CreateElement("url");
            urlNode.InnerText = url;
            feedNode.AppendChild(urlNode);
            rootNode.AppendChild(feedNode);
            doc.Save(fileName);
            }
            catch (Exception ex) {
              Console.WriteLine(ex.InnerException.Message);
            }

        }

        public static void RemoveFeed (string innerName, string fileName) {
            try{
            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);            //name node ("feeds/feed/name[text()='"+innerName+"']");
            XmlNode feedNode=doc.SelectSingleNode("feeds/feed[name='"+innerName+"']");
            XmlNode rootNode=feedNode.ParentNode;
            rootNode.RemoveChild(feedNode);
            //XmlNode feedNode=doc.SelectSingleNode("descendant::feed[name=innerName]");
            Console.WriteLine(feedNode.InnerText);
            doc.Save(fileName);
            }
            catch (Exception ex) {
              Console.WriteLine(ex.Message);
            }
        }

        public static void Download (string [] feedNames, string fileName) {
            List<string> urls=new List<string>();
            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);
            if (feedNames.Length==0) {
              Console.WriteLine("Feeds NOT DEFINED");
              if (GetAllFeedUrls(fileName).Length==0)
              {
                Console.WriteLine("There are also no feeds in xml config file");
                return;
              }
              //if no feeds were defined for downloads all urls are downloaded
              feedNames=GetAllFeedNames(fileName);
            }
            Console.WriteLine(feedNames.Length);
            Dictionary<string,string> nameUrlDict=new Dictionary <string,string>();
            foreach(string fn in feedNames) {
                 string urlPath=getFeedUrl(fn,doc);
            urls.Add(urlPath);
            nameUrlDict.Add(urlPath,fn);
            }
            string urlList=String.Join(',',urls);
            Console.WriteLine("Feeds urls:"+urlList);
            List<Task> tasks=new List<Task>();
            foreach(string url in urls) {
              //getFeedContent(url,doc,nameUrlDict);
              //Task<SyndicationFeed> task=getFeedTask(url,doc,nameUrlDict);
              tasks.Add(getFeedTask(url,doc,nameUrlDict));
              //SyndicationFeed feed=task.Result;
              //Console.WriteLine("Title: "+feed.Title.Text);
              //foreach(SyndicationItem item in feed.Items) {
              //Console.WriteLine("News Title: "+item.Title.Text);
              //}
            }
            Task.WaitAll(tasks.ToArray());
        }

        public static string getFeedUrl(string name, XmlDocument doc) {
            XmlNode feedNode=doc.SelectSingleNode("feeds/feed[name='"+name+"']");
            string feedUrl=feedNode["url"].InnerText;
            return feedUrl;
        }

        public static async Task getFeedTask(string url,XmlDocument doc, Dictionary<string,string> nameUrlDict) {
              await Task.Run(()=>
              getFeed(url,doc,nameUrlDict));
        }

        public static void getFeed(string url,XmlDocument doc, Dictionary<string,string> nameUrlDict) {
            Console.WriteLine("Downloading feed for "+nameUrlDict[url]);
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            SyndicationFeed feed=null;
            try{
            using (XmlReader reader=XmlReader.Create(url)) {
            feed=SyndicationFeed.Load(reader);
            Console.WriteLine("Title: "+feed.Title.Text);
            foreach(SyndicationItem item in feed.Items) {
                Console.WriteLine("News Title: "+item.Title.Text);
            }
            }
            }
            catch (Exception ex) {
              Console.WriteLine("22"+ex.ToString());
            }
            //return feed;
        }

        public static void getFeedContent(string url,XmlDocument doc, Dictionary<string,string> nameUrlDict) {
            Console.WriteLine("Downloading feed for "+nameUrlDict[url]);
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            try{
            using (XmlReader reader=XmlReader.Create(url)) {
            SyndicationFeed feed=SyndicationFeed.Load(reader);
            Console.WriteLine("Feed Title: "+feed.Title.Text);
            foreach(SyndicationItem item in feed.Items) {
                Console.WriteLine("News Title: "+item.Title.Text);
            }
            }
            }
            catch (Exception ex) {
              Console.WriteLine(ex.Message);
              Console.WriteLine("22"+ex.ToString());
            }
        }

        public static string[] GetAllFeedNames (string fileName) {
            List<string> names=new List<string>();
            try{
            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);            //name node ("feeds/feed/name[text()='"+innerName+"']");
            XmlNodeList feedNodeList=doc.SelectNodes("feeds/feed");
            names=feedNodeList.Cast<XmlNode>().Select(n=>n["name"].InnerText).ToList();
            names.ForEach(n=>Console.WriteLine(n));
            }
            catch (Exception ex) {
              Console.WriteLine(ex.Message);
            }
            return names.ToArray();
        }

        public static string[] GetAllFeedUrls (string fileName) {
            List<string> urls=new List<string>();
            try{
            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);
            XmlNodeList feedNodeList=doc.SelectNodes("feeds/feed");
            urls=feedNodeList.Cast<XmlNode>().Select(n=>n["url"].InnerText).ToList();
            urls.ForEach(n=>Console.WriteLine(n));
            }
            catch (Exception ex) {
              Console.WriteLine(ex.Message);
            }
            return urls.ToArray();
        }
    }
}